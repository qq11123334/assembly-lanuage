#include <stdio.h>
#include <xmmintrin.h>
#include <time.h>
typedef struct timespec timespec;
timespec diff(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}
int main()
{
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
    
    float data[200][202];
    FILE *input_file = fopen("data.txt", "r");

    
    int n = 200, m = 202;
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < m;j++)
        {
            fscanf(input_file, "%f", &data[i][j]);
        }
    }
    fclose(input_file);

    FILE *output_file = fopen("output_for_noSIMD.txt", "w");

    for(int row_base = 0;row_base < n;row_base++)
    {
        float sum = 0;
        for(int row = 0;row < n;row++)
        {
            for(int col = 0;col < m;col++)
            {
                sum += (data[row_base][col] * data[row][col]);
            }
        }
        fprintf(output_file, "%f\n", sum);
    }

    fclose(output_file);

    clock_gettime(CLOCK_MONOTONIC, &time2);

    printf("No  SIMD %f sec\n", diff(time1, time2).tv_nsec / 1e9);
    return 0;
}
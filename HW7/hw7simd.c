#include <stdio.h>
#include <xmmintrin.h>
#include <time.h>
typedef struct timespec timespec;
timespec diff(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}
int main()
{
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);

    float data[200][204] __attribute__ ((aligned(16)));
    __m128 *data_m[200];
    FILE *input_file = fopen("data.txt", "r");

    int n = 200, m = 202;
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < m;j++)
        {
            fscanf(input_file, "%f", &data[i][j]);
        }
        if(m % 4 >= 1) data[i][m] = 0;
        if(m % 4 >= 2) data[i][m + 1] = 0;
        if(m % 4 >= 3) data[i][m + 2] = 0;
    }

    fclose(input_file);

    FILE *output_file = fopen("output_for_using_SIMD.txt", "w");

    for(int i = 0;i < n;i++)
        data_m[i] = (__m128 *)data[i];

    int times_for_col = (m / 4) + (m % 4 != 0);
    for(int row_base = 0;row_base < n;row_base++)
    {
        __m128 sum;
        sum = _mm_sub_ps(sum, sum);
        for(int row = 0;row < n;row++)
        {
            for(int col = 0;col < times_for_col;col++)
            {
                sum += (data_m[row_base][col] * data_m[row][col]);
                // sum += _mm_mul_ps(data_m[row_base][col], data_m[row][col]);
            }
        }
        float *ptr = (float *)&sum;
        float f_sum = ptr[0] + ptr[1] + ptr[2] + ptr[3];
        fprintf(output_file, "%f\n", f_sum);
    }

    fclose(output_file);

    clock_gettime(CLOCK_MONOTONIC, &time2);

    printf("Use SIMD %f sec\n", diff(time1, time2).tv_nsec / (1e9));
    return 0;
}
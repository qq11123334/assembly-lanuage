/* ========================= */
/*       DATA section        */
/* ========================= */
    .data
    .align 4
/* --- variable a --- */
    .type a,%object
    .size a,48
a:
    .word 1
    .word 1
    .word 2
    .word 3

    .word 3
    .word 4
    .word 2
    .word 1
	
    .word 1
    .word 2
    .word 2
    .word 1    
/* --- variable b --- */
    .type b, %object
    .size b, 48
b:
    .word 1
    .word 2
    .word 3
    
    .word 3
    .word 2
    .word 1
    
    .word 0
    .word 1
    .word 0
    
    .word 1
    .word 1
    .word 2

/* --- variable c --- */
    .type c, %object
    .size c, 36
c:
    .space 36, 0

/* ========================= */
/*       TEXT section        */
/* ========================= */
    .section .text
    .global main
    .type main,%function
main:
    ldr r0, =a /* point to the first element of each row of A */
    ldr r1, =b /* point to the first element of each column of B */
    ldr r2, =c /* point to every element of C */

    mov r10, #0 /* counter for the current row of C */
    mov r11, #0 /* counter for the current column of C */
    mov r12, #0 /* when computing C(r10, r11) = the sum of A(r10, r12) * B(r12, r11) and r12 = 0 ~ 3 */
    mov r3, #0 /* initialize */

Loop1:
    /* Loop1 compute the each result of r10 row and r10 will be (0 ~ 2) */

    mov r11, #0 /* initialize */
Loop2:
    /* Loop2 compute the each result of r11 column with r10 row and r11 will be (0 ~ 2) */
    /* i.e. compute C(r10, r11) */

    mov r12, #0 /* initialize */
Loop3:
    /*Loop3 compute the result of every A(r10, r12) * B(r12, r11) and store in r6*/
    ldr r4, [r0, r12, LSL #2] /* r4 = A[r10][r12] */
    mov r7, #12
    mul r8, r12, r7
    ldr r5, [r1, r8] /* r5 = B[r12][r11] */
    mul r6, r4, r5 /* r6 = r5 * r4 */

    
    add r3, r3, r6 /* add A(r10, r12) * B(r12, r11) to s3, so the result of C(r10, r11) will be in r3 after Loop3 */

    add r12, r12, #1 /* update r12 */
    cmp r12, #4 /* if r12 == 4, implies r3 have been equal C(r10, r11), break and move ro next column*/
    bne Loop3

    /*----Loop3----*/

    nop
    str r3, [r2], #4 /* store r3 to C */
    add r1, r1, #4 /* update r1 to the next column of B */
    mov r3, #0 /* clear r3 */


    add r11, r11, #1 /* update r11 to next column of C*/
    cmp r11, #3 /* if r11 == 3, implies this column is over, break and move to next row */
    bne Loop2
    
    /*----Loop2----*/

    ldr r1, =b /* initialize the column of B*/
    add r0, r0, #16 /* update r0 to the current row of A */

    add r10, r10, #1 /* update r10 to the next row of C */
    cmp r10, #3 /* if r10 == 3, implies that this program is over, break */
    bne Loop1

    /*----Loop1----*/

    ldr r1, =c /* let r1 point to the address of C's first element */

    nop
.end

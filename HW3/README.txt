我的方法是寫三迴圈,
最裡面那個迴圈,將A(i, k) * B(k, j) (此處的k為r12)的結果存進r6,並加到r3裡,算出C(i, j),也就是最後r3的值
而第二個內層迴圈跑過所有j的值,也就是r11,
最外層的迴圈跑過所有i的值,也就是r10,
其他就只是暫存位置或值的功用,
r0會指向A每個row的第一個元素,
r1會指向B每個column的第一個元素,
r2會指向C的每個元素,並將r3的值存進去

最後將r1指向C的開頭元素

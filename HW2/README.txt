程式內容:
    一開始先將開頭打完,
    在用MOV去指定r1, r2, r3的值,
    然後讓r0 = r2 + r2 * 2其實就是r0 = 3 * r2,
    在讓r0 = r0 + r1 * 2和r0 = r0 + r3 * 4,
    這樣r0最終的值就會是2 * r1 + 3 * r2 + 4 * r3了

編譯執行:
    寫完之後就打指令編譯,並且在Ubuntu20.04用insight執行,但遇到ctr0.s的問題,
    所以就去下載新編譯的arm cross toolchain,但還是有相同的問題,
    所以就到Console執行,也成功執行,
    在insight裡就一直輸入n,並觀察register值的變化

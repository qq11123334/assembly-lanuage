    .section .text
    .global main
    .type main, %function
main:
    MOV r1, #2
    MOV r2, #3
    MOV r3, #4
    ADD r0, r2, r2, LSL #1
    ADD r0, r0, r1, LSL #1
    ADD r0, r0, r3, LSL #2
    NOP
    .end

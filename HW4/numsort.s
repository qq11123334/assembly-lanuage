
    .section .text
    .global NumSort
    .type NumSort, %function

NumSort:
    /*
    initialisze in main
    r1: the address of old array
    r0: the number of numbers in array
    r3: the address of result array
    */

    MOV ip, sp
    STMFD sp!, {r0-r10, fp, ip, lr, pc}
    SUB fp, ip, #4

    MOV r4, #0 /* r4 will be 0, 4, 8, 12 ...(r0 - 1) * 4 counting for Copy_array */ 
Copy_array:

    /*the first number is at r1 + 0, second numnber is at r1 + 4, third is at r1 + 8, r1 + 12...*/
    /*so the ith number is at r1 + r4*/
    LDR r5, [r1, r4] /* load the number in old array */
    STR r5, [r3, r4] /* store the number to result array */

    ADD r4, #4 /*update r4 */
    CMP r4, r0, LSL #2 /* check if copying is finished*/
    BLT Copy_array /* Beacuse the last number is at r1 + 4 * (r0 - 1), if r4 < 4 * r0, it's not complete, and continue the loop*/



    /* bubble sort */

    MOV r4, #0 /* r4 will be 0, 1, 2, 3 ... counting for Loop_r4 */ 
Loop_r4: /* let Loop_r5 sweep r0 times */
    
    
    MOV r5, #0 /* r5 will be 0, 4, 8, 12 ... (r0 - 2) * 4 counting for Loop_r5 */ 
Loop_r5: /* check all adjacent number */

    /* ith number is at r3 + r5 */
    LDR r6, [r3, r5] /* load the ith number */
    ADD r8, r5, #4 /*r8 = r5 + 4 */
    LDR r7, [r3, r8] /* load the i+1th number*/

    /*compare */
    CMP r6, r7 /*compare adjacent number*/
    
    /*if the front number is less than its next number, do swap */
    /*change each other's value into their memory*/
    STRLT r6, [r3, r8]     /*swap */
    STRLT r7, [r3, r5]
    
    SUB r2, r0, #1 /*r2 = r0 - 1 */
    ADD r5, #4 /*update r5 */
    CMP r5, r2, LSL #2 /*check if r5 is out of range of array(if r5 < (r0 - 1) * 4)) */
    BLT Loop_r5 /*if it's not out of range, continue Loop_r5 */

    ADD r4, #1 /*update r4*/
    CMP r4, r0 /*check how many times does Loop_r5 do */
    BLT Loop_r4 /* if it's not over r0 times, continue Loop_r4 let Loop_r5 run again */

    NOP
    LDMEA fp, {r0-r10, fp, sp, pc}
    .end

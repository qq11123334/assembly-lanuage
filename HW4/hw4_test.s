    .data
    .align 4
/* --- variable a --- */
    .type a,%object
    .size a, 48
a:
    .word 87
    .word 86
    .word 88
    .word 12

    .word 2
    .word 3
    .word 112
    .word 31
	
    .word 1
    .word 5
    .word 8
    .word 99


    .type c, %object
    .size c, 48
c:
    .space 48, 0

    .section .text
    .global main
    .type main, %function

main:
    MOV ip, sp
    STMFD sp!, {fp, ip, lr, pc}
    SUB fp, ip, #4

    LDR r1, =a
    LDR r3, =c /* c is the result array */
    MOV r0, #12 /*the number of numbers in array */

    BL NumSort

    NOP
    LDMEA fp, {fp, sp, pc}
    .end

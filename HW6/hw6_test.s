.set SWI_Write, 0x5
.set SWI_Open, 0x1
.set SWI_Close, 0x2
.set AngelSWI, 0x123456

/* ========================= */
/*       DATA section        */
@ /* ========================= *
	.data
	.align 4
file_fd:
	.space 4 

open_mode:
	.ascii "w\0"

an_int:
	.ascii "%d \0"

filename:
	.ascii "result.txt\000"

string:
	.space 200

tmp:
	.ascii "\0"
/* ========================= */
/*       TEXT section        */
/* ========================= */
	.section .text
	.global main
	.type main,%function
.string_in_main:
	.word string
.open_file:
	.word filename
	.word 0x4
	.word 0x8
.write_file:
	.space 4
	.space 4
	.space 4
.close_file:
	.space 4
main:
	mov ip, sp
	stmfd sp!, {fp, ip, lr, pc}
	sub fp, ip, #4

	ldr r0, =1233
	bl srand /* 呼叫srand(1233)設定亂數種子 */

	mov r0, #32 
	bl malloc /* malloc(sizeof(int) * 8) */
	
	mov r6, r0  /* 讓r6指向陣列開頭位置 */
	mov r5, #0  /* for迴圈的i r5 = 0, 4, 8 ... 32 */

/* 隨機產生亂數存進陣列*/
Loop_r5_1:

	bl rand /* 亂數產生陣列的值 */
	mov r0, r0 , LSR #20 /* 讓數字小一點 */
	str r0, [r6, r5] /* 存進陣列 */

	add r5, r5, #4 /* r5 += 4 */
	cmp r5, #32 /* 若執行八次了 跳出迴圈 */
	blt Loop_r5_1

	mov r0, #8 /* 陣列size */
	mov r1, r6 /* 開頭位置 */
	bl NumSort /* numsort(size, address) */

	mov r5, #0 /* for迴圈的i */
	mov r6, r0 /* 新陣列的開頭位置 存進r6 */

/* 將陣列裡的數字轉為字串存到string裡 */
Loop_r5_2:

	ldr r0, =tmp /* tmp為暫存字串 */
	ldr r1, =an_int /* an_int 為 "%d " -> sprintf裡面要的format*/
	ldr r2, [r6, r5] /* 把陣列的值load進r2 */
	bl sprintf /* 把數字用字串的形式存到tmp裡 */

	ldr r0, =string /* string為答案字串 */
	ldr r1, =tmp 
	bl strcat /* 用 strcat(string, tmp) 加在後面*/

	add r5, r5, #4
	cmp r5, #32
	blt Loop_r5_2 /* for迴圈的i */

	mov r0, #SWI_Open /*開檔案 */
	adr r1, .open_file
	swi AngelSWI
	mov r2, r0 /* 將file descriptor簡稱fd 存在r2 */

	adr r1, .write_file /* 將讀寫資訊寫進.write_file 也就是r2 */
	str r2, [r1, #0] /* 將fd存進去  */
	
	ldr r0, .string_in_main
	str r0, [r1, #4] /* 將欲寫入的string開頭位址存進去 */

	ldr r0, =string
	bl strlen /* 呼叫strlen(string) */

	adr r1, .write_file
	str r0, [r1, #8] /* 將字串長度存進去 */

	mov r0, #SWI_Write /* 將指令寫進r0 */
	swi AngelSWI /* semihosting寫入 */

	adr r1, .close_file /* 關檔案 */
	str r2, [r1]
	mov r0, #SWI_Close
	swi AngelSWI

	@ ldr r0, =filename
	@ ldr r1, =open_mode
	@ bl fopen
	
	@ ldr r10, =file_fd
	@ str r0, [r10]
	@ ldr r1, =string
	@ bl fprintf

	@ ldr r0, [r10]
	@ bl fclose
	
	nop

	ldmea fp, {fp, sp, pc}
	.end

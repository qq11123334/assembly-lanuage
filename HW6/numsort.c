#include <stdio.h>
#include <stdlib.h>

/* bubble sort */
int *NumSort(int size, int *arr)
{
    int *new_arr = malloc(size * sizeof(int));

    for(int i = 0 ;i < size;i++) new_arr[i] = arr[i];

    for(int i = 0;i < size;i++)
    {
        for(int j = 0;j < size - 1;j++)
        {
            if(new_arr[j] < new_arr[j + 1])
            {
                int tmp = new_arr[j];
                new_arr[j] = new_arr[j + 1];
                new_arr[j + 1] = tmp;
            }
        }
    }

    return new_arr;
}
